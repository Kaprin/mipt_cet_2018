﻿#include <iostream>
#include <cmath>

// Класс комплексных чисел
struct Complex {
private:
	double re;	// действительная часть
	double im;	// мнимая часть
public:
	Complex() { 
	}
	Complex(double val) { 
		re = val;
		im = 0;
	}
	Complex(double real, double imaginary) {
		re = real;
		im = imaginary;
	}
	// констурктор копирования
	Complex(const Complex& val) { 
		re = val.re;
		im = val.im;
	}
	// оператор присваивания
	Complex operator= (const Complex& val) {
		re = val.re;
		im = val.im;
		return (*this);
	}
	// возвращает действительную часть числа
	double real() {
		return re;
	}
	// возвращает мнимую часть числа 
	double image() {
		return im;
	}
	// модуль комплексного числа
	double abs() { 
		return (re * re + im * im);
	}
	// сложение комплексных чисел
	Complex operator+(const Complex& val) {
		return Complex(re + val.re, im + val.im);
	}
	// вычитание комплексного числа
	Complex operator-(const Complex& val) {
		return Complex(re - val.re, im - val.im);
	}
	// умножение комплесных чисел
	Complex operator*(const Complex& val) {
		return Complex(re*val.re - im*val.im,im*val.re + re * val.im);
	}
	// деление комплексных чисел
	Complex operator/(const Complex& val) {
		double r = (re*val.re + im * val.im) / (val.re*val.re * val.im*val.im);
		double i = (im*val.re - re*val.im) / (val.re*val.re * val.im*val.im);
		return Complex(r, i);
	}
	// оператор комплексного сопряжения
	Complex operator~() {
		return Complex(re, -im);
	}
	// унарный оператор -
	Complex operator-() {
		return Complex(-re, -im);
	}
	friend std::ostream& operator<< (std::ostream &, const Complex &);
	friend std::istream& operator>> (std::istream &, Complex &);
};

// перегрузка оператора вывода
std::ostream& operator<< (std::ostream &out, const Complex& val) {
	out << "Re = " << val.re << ", Im = " << val.im;
	return out;
}

// перегрузка оператора ввода
std::istream& operator>> (std::istream &in, Complex& val) {
	in >> val.re >> val.im;
	return in;
}

int main() {
	Complex a(3, 5);
	Complex b(7, 4);
	Complex c(a);
	Complex d = b;

	std::cout << "a = " << a << std::endl;
	std::cout << "b = " << b << std::endl;
	std::cout << "c = " << c << std::endl;
	std::cout << "c = " << d << std::endl;
	std::cout << "Abs(a) =  " << a.abs() << std::endl;
	std::cout << "b.im = " << b.image() << " b.re = " << b.real() << std::endl;
	std::cout << "b - a : " << b - a << std::endl;
	std::cout << "a + b : " << a + b << std::endl;
	std::cout << "a * b : " << a * b << std::endl;
	std::cout << "a / b : " << a / b << std::endl;
	std::cout << "complex-conjugate for d: " << ~d << std::endl;
	system("pause");
	return 0;
}